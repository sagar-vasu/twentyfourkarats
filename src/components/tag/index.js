import React from "react";
import "./tag.css";

class Tag extends React.Component {
  render() {
    return (
      <button className={this.props.className} onClick={this.props.onClick}>
        {this.props.name}
      </button>
    );
  }
}

export default Tag;
