import React from "react";
import "./search.css";
// icon from react-icon
import { FaArrowLeft, FaSearch } from "react-icons/fa";
// components
import { Tag } from "../../components";

class SearchScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      price: "",
      specie: "",
      species: [
        { specie: "Indica", className: "tagButton" },
        { specie: "Hybrid", className: "tagButton" },
        { specie: "Sativa", className: "tagButton" }
      ],
      prices: [
        { price: "$", dollors: 20, className: "tagButton" },
        { price: "$$", dollors: 49, className: "tagButton" },
        { price: "$$$", dollors: 50, className: "tagButton" }
      ]
    };
  }

  goBack = () => {
    this.props.history.goBack();
  };

  moveOnNextScreen = () => {
    let { specie, price } = this.state;
    this.props.history.push("/search_result", {
      specie,
      price
    });
  };

  checkTag = value => {
    let { species } = this.state;
    for (var i = 0; i < species.length; i++) {
      if (species[i].specie === value.specie) {
        species[i].className = "tagButtonAfter";
        this.setState({
          species,
          specie: value.specie
        });
      } else {
        species[i].className = "tagButton";
        this.setState({
          species
        });
      }
    }
  };

  checkPrice = value => {
    let { prices } = this.state;
    for (var i = 0; i < prices.length; i++) {
      if (prices[i].price === value.price) {
        prices[i].className = "tagButtonAfter";
        this.setState({
          prices,
          price: value.dollors
        });
      } else {
        prices[i].className = "tagButton";
        this.setState({
          prices
        });
      }
    }
  };

  render() {
    let { species, prices } = this.state;
    return (
      <div className="searchContainer">
        <div className="searchHeader">
          <div className="searchBack" onClick={() => this.goBack()}>
            <FaArrowLeft className="searchIcon" size={30} color="#783011" />
            <h4>Back</h4>
          </div>
          <h1>SEARCH</h1>
        </div>
        <div className="searchBody">
          <input type="search" className="searchInput" placeholder="NAME" />
          <div className="searchButtons">
            {species &&
              species.map((val, i) => {
                return (
                  <Tag
                    key={i}
                    name={val.specie}
                    className={val.className}
                    onClick={() => this.checkTag(val)}
                  />
                );
              })}
          </div>
          <div className="searchButtons">
            {prices &&
              prices.map((val, i) => {
                return (
                  <Tag
                    key={i}
                    name={val.price}
                    className={val.className}
                    onClick={() => this.checkPrice(val)}
                  />
                );
              })}
          </div>
        </div>

        <div className="searchfooter">
          <div className="searchHome" onClick={() => this.moveOnNextScreen()}>
            <FaSearch className="searchIcon" size={30} color="#783011" />
            <h2>SEARCH</h2>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchScreen;

// <div className="searchButtons">
//         <button
//           className={
//             this.state.specie === "CBC" ? "searchItemAfter" : "searchItem"
//           }
//           onClick={() =>
//             this.setState({
//               specie: "CBC"
//             })
//           }
//         >
//           CBC
//         </button>

//         <button
//           className={
//             this.state.specie === "Hybrid"
//               ? "searchItemAfter"
//               : "searchItem"
//           }
//           onClick={() =>
//             this.setState({
//               specie: "Hybrid"
//             })
//           }
//         >
//           Hybrid
//         </button>

//         <button
//           className={
//             this.state.specie === "Sativa"
//               ? "searchItemAfter"
//               : "searchItem"
//           }
//           onClick={() =>
//             this.setState({
//               specie: "Sativa"
//             })
//           }
//         >
//           Sativa
//         </button>
//       </div>

//       <div className="searchButtons">
//         <button
//           className={
//             this.state.price === "20"
//               ? "searchItemPriceAfter"
//               : "searchItemPrice"
//           }
//           onClick={() =>
//             this.setState({
//               price: "20"
//             })
//           }
//         >
//           $
//         </button>
//         <button
//           className={
//             this.state.price === "49.99"
//               ? "searchItemPriceAfter"
//               : "searchItemPrice"
//           }
//           onClick={() =>
//             this.setState({
//               price: "49.99"
//             })
//           }
//         >
//           $$
//         </button>

//         <button
//           className={
//             this.state.price === "50.00"
//               ? "searchItemPriceAfter"
//               : "searchItemPrice"
//           }
//           onClick={() =>
//             this.setState({
//               price: "50.00"
//             })
//           }
//         >
//           $$$
//         </button>
//       </div>
//     </div>
