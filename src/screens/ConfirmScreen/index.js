import React from "react";
import "./confirm.css";
// icon from react-icon
import { FaArrowLeft, FaHome } from "react-icons/fa";
// image from assets folder
import Logo from "../../assets/logo.png";

class ConfirmScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      userData: {}
    };
  }

  goBack = () => {
    this.props.history.push("/");
  };

  componentDidMount() {
    this.setState({
      userData: this.props.location.state
    });
  }

  render() {
    let { userData } = this.state;
    return (
      <div className="confirmContainer">
        <div className="confirmHeader">
          <div className="confirmBack" onClick={() => this.goBack()}>
            <FaArrowLeft className="confirmIcon" size={30} color="#783011" />
            <h4>Back</h4>
          </div>
          <div style={{ flexDirection: "column" }}>
            <h1>THANK YOU!</h1>
            <h4 style={{ marginLeft: "20px" }}>from 24 Karats</h4>
          </div>
        </div>

        <div className="confirmBody">
          <h3>We have emailed you a confirmation</h3>
          <img src={Logo} alt="logo" className="confirmLogo" />
          <h4>
            Your order number is : <b className="confirmInfo">H3882</b>
          </h4>
          <h4>
            Your order is for :{" "}
            <b className="confirmInfo">{userData && userData.name}</b>
          </h4>
          <h4>
            Your order total is :
            <b className="confirmInfo">{userData && userData.totalPrice}</b>
          </h4>
          <span>plus applicable taxes</span>
          <div>
            <span>
              Price in -app are for estimating purpose onl, due to possibility
              of software error or delay between app and in- store systems. In
              the event of a diffrence between app and in store prices, in store
              prices will be applied.
            </span>
          </div>
        </div>
        <div className="confirmfooter">
          <div
            className="confirmHome"
            onClick={() => this.props.history.push("/")}
          >
            <FaHome className="confirmIcon" size={30} color="#783011" />
            <h2>OK</h2>
          </div>
        </div>
      </div>
    );
  }
}

export default ConfirmScreen;
