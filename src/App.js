import React from "react";
import MainAppNavigation from "./config/navigation";

class App extends React.Component {
  render() {
    return <MainAppNavigation />;
  }
}
export default App;
