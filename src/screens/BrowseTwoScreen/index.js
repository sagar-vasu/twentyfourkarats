import React from "react";
import "./browsetwo.css";
// icon from react-icons
import { FaArrowLeft } from "react-icons/fa";
// component
import { List } from "../../components";

class BrowseTwoScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      headerData: {}
    };
  }

  goBack = () => {
    this.props.history.goBack();
  };

  async componentDidMount() {
    this.setState({
      headerData: this.props.location.state,
      loading: true
    });
    await fetch("http://24k.etangle.ca/product-display.json")
      .then(response => response.json())
      .then(async json => {
        let allData = json.data;
        var filteredArray = [];
        (await allData) &&
          allData.filter(values => {
            if (
              values &&
              values.category &&
              values.category._id &&
              values.category._id === this.state.headerData._id
            ) {
              let potency = values.potency;
              let myRequiredData = {
                strain: values.name ? values.name : "----",
                specie: values.species ? values.species.name : "----",
                description: values.description ? values.description : "----",
                brand:
                  values.brand && values.brand.name
                    ? values.brand.name
                    : "----",
                price: values.price,
                potency: values.potency,
                weight: `${values.weight.amount} ${values.weight.unit} `,
                thc: `${potency[0].min === null ? 0 : potency[0].min}-${
                  potency[0].max === null ? 0 : potency[0].max
                }`,
                cbd: `${potency[1].min === null ? 0 : potency[1].min}-${
                  potency[1].max === null ? 0 : potency[1].max
                }`
              };
              filteredArray.push(myRequiredData);
              this.setState({ filteredArray, loading: false });
            }
          });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    let { headerData, filteredArray } = this.state;
    return (
      <div>
        <div className="browseTwoHeader">
          <div className="browseTwoBack" onClick={() => this.goBack()}>
            <FaArrowLeft className="browseTwoIcon" size={30} color="#783011" />
            <h4>Back</h4>
          </div>
          <h4>{headerData.name}</h4>
        </div>
        <div style={{ marginBottom: "100px" }}>
          {this.state.loading ? (
            <div style={{ textAlign: "center", marginTop: "80px" }}>
              <h1 style={{ color: "white" }}>Loading.......</h1>
            </div>
          ) : (
            <table className="browseTwoTable">
              <thead>
                <tr>
                  <th scope="col" className="browseTwoHeadings">
                    STRAIN
                  </th>
                  <th scope="col" className="browseTwoHeadings">
                    BRAND
                  </th>
                  <th scope="col" className="browseTwoHeadings">
                    TYPE
                  </th>
                  <th scope="col" className="browseTwoHeadings">
                    CBD
                  </th>
                  <th scope="col" className="browseTwoHeadings">
                    THC
                  </th>
                  <th scope="col" className="browseTwoHeadings">
                    AMT
                  </th>
                  <th scope="col" className="browseTwoHeadings">
                    PRICE
                  </th>
                </tr>
              </thead>
              <tbody>
                {filteredArray &&
                  filteredArray.map((val, i) => {
                    return (
                      <List
                        onClick={() =>
                          this.props.history.push("/item_details", val)
                        }
                        key={i}
                        data={val}
                      />
                    );
                  })}
              </tbody>
            </table>
          )}
        </div>
      </div>
    );
  }
}

export default BrowseTwoScreen;
