import React from "react";
import "./404.css";

class Notfound extends React.Component {
  render() {
    return (
      <div className="notfound">
        <h1>Sorry! Page not found</h1>
      </div>
    );
  }
}

export default Notfound;
