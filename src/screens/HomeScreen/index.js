import React from "react";
import "./home.css";

//   image from assets folder
import Logo from "../../assets/logo.png";

//   icons from  react-icons

import {
  FaCanadianMapleLeaf,
  FaShoppingBasket,
  FaSearch
} from "react-icons/fa";

class HomeScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      screen: "Home Screen"
    };
  }

  //   card one function
  List = () => {
    this.props.history.push("browse_one");
  };

  //   card two function

  Find = () => {
    this.props.history.push("search");
  };

  //   card three function

  Order = () => {
    this.props.history.push("my_order");
  };

  render() {
    return (
      <div className="homeContainer">
        <img src={Logo} alt="logo" className="homeLogo" />
        <div className="homeCards">
          <div className="homeItem" onClick={() => this.List()}>
            <FaCanadianMapleLeaf
              className="homeIcon"
              size={70}
              color="#783011"
            />
            <h1>LIST</h1>
          </div>
          <div className="homeItem" onClick={() => this.Order()}>
            <FaShoppingBasket className="homeIcon" size={70} color="#783011" />
            <h1>ORDER</h1>
          </div>
          <div className="homeItem" onClick={() => this.Find()}>
            <FaSearch className="homeIcon" size={70} color="#783011" />
            <h1>FIND</h1>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeScreen;
