import React from "react";
import "./myorder.css";
import { FaArrowLeft, FaCheck } from "react-icons/fa";
import { List, Tag } from "../../components";

export default class Myorder extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      phone: "",
      address: "",
      instruction: "",
      payment: "",
      products: [],
      changeClass: "orderBtn",
      types: [
        { type: "PICKUP", className: "tagButton" },
        { type: "DELIVER", className: "tagButton" }
      ],
      payments: [
        { type: "CASH", className: "tagButton" },
        { type: "DEBIT", className: "tagButton" },
        { type: "CREDIT", className: "tagButton" }
      ]
    };
  }

  goBack = () => {
    this.props.history.goBack();
  };

  componentDidMount() {
    let getData = localStorage.getItem("products");
    if (getData) {
      var totalPrice = 0;
      let products = JSON.parse(getData);
      for (let i = 0; i < products.length; i++) {
        totalPrice = products[i].price + totalPrice;
        console.log(totalPrice);
      }
      this.setState({
        products,
        totalPrice: totalPrice.toFixed(2)
      });
    }
  }

  submit = () => {
    let {
      name,
      email,
      phone,
      address,
      instruction,
      totalPrice,
      option,
      payment
    } = this.state;

    if (!name) {
      alert("Name is required");
    } else if (!email) {
      alert("Email is required");
    } else if (!phone) {
      alert("Phone is required");
    } else if (!address) {
      alert("Address is required");
    } else if (!instruction) {
      alert("Address is required");
    } else {
      let data = {
        name,
        email,
        phone,
        address,
        instruction,
        totalPrice,
        option,
        payment
      };
      console.log(data, "here is form data");
      this.props.history.push("/confirm", this.state);
      localStorage.removeItem("products");
    }
  };

  checkPayment = value => {
    let { payments } = this.state;
    for (var i = 0; i < payments.length; i++) {
      if (payments[i].type === value.type) {
        payments[i].className = "tagButtonAfter";
        this.setState({
          payments,
          payment: value.type
        });
      } else {
        payments[i].className = "tagButton";
        this.setState({
          payments,
          option: value.type
        });
      }
    }
  };

  checkType = value => {
    let { types } = this.state;
    for (var i = 0; i < types.length; i++) {
      if (types[i].type === value.type) {
        types[i].className = "tagButtonAfter";
        this.setState({
          types,
          option: value.type
        });
      } else {
        types[i].className = "tagButton";
        this.setState({
          types,
          option: value.type
        });
      }
    }
  };

  render() {
    let { types, payments } = this.state;
    return (
      <div className="_container">
        {/* header */}
        <div className="orderHeader">
          <div className="orderBack" onClick={() => this.goBack()}>
            <FaArrowLeft className="orderIcon" size={30} color="#783011" />
            <h4>Back</h4>
          </div>
          <h1>MY ORDER</h1>
        </div>

        {/* section one */}
        <div className="_mainDiv">
          <table className="orderTable">
            <thead>
              <tr>
                <th scope="col" className="orderHeadings">
                  STRAIN
                </th>
                <th scope="col" className="orderHeadings">
                  BRAND
                </th>
                <th scope="col" className="orderHeadings">
                  TYPE
                </th>
                <th scope="col" className="orderHeadings">
                  CBD
                </th>
                <th scope="col" className="orderHeadings">
                  THC
                </th>
                <th scope="col" className="orderHeadings">
                  AMT
                </th>
                <th scope="col" className="orderHeadings">
                  PRICE
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.products &&
                this.state.products.map((val, i) => {
                  return <List data={val} key={i} />;
                })}
            </tbody>
          </table>

          {/* section two */}
          <section>
            <div className="_tab">
              {types &&
                types.map((val, i) => {
                  return (
                    <Tag
                      key={i}
                      name={val.type}
                      className={val.className}
                      onClick={() => this.checkType(val)}
                    />
                  );
                })}
            </div>

            <div className="_form">
              <input
                onChange={text => this.setState({ name: text.target.value })}
                placeholder="YOUR NAME"
                className="_input"
                value={this.state.name}
              />
              <input
                onChange={text => this.setState({ email: text.target.value })}
                placeholder="YOUR EMAIL"
                className="_input"
                value={this.state.email}
              />
              <input
                onChange={text => this.setState({ phone: text.target.value })}
                placeholder="YOUR PHONE #"
                className="_input"
                value={this.state.phone}
                type="number"
              />
              <input
                onChange={text => this.setState({ address: text.target.value })}
                placeholder="ADDRESS"
                className="_input"
                value={this.state.address}
              />
              <input
                onChange={text =>
                  this.setState({
                    instruction: text.target.value
                  })
                }
                placeholder="OTHER INSTRUCTIONS"
                className="_input"
                value={this.state.instruction}
              />
            </div>
          </section>

          <div className="_tab">
            {payments &&
              payments.map((val, i) => {
                return (
                  <Tag
                    key={i}
                    name={val.type}
                    className={val.className}
                    onClick={() => this.checkPayment(val)}
                  />
                );
              })}
          </div>

          {/* section three */}
          <section className="_section_three">
            <h5>
              Your order detail is:{" "}
              <span className="_price">${this.state.totalPrice} </span>
            </h5>
            <p>plus application taxes,</p>
            <p>
              Price in -app are for estimating purposes on, due to possibility
              of software error or delay between app and in store ststem, in the
              event of a difference between app and in-store prices, in store
              price will be applied.
            </p>
          </section>

          <div className="orderfooter">
            <div className="orderHome" onClick={() => this.submit()}>
              <FaCheck className="orderIcon" size={30} color="#783011" />
              <h2>ORDER</h2>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
