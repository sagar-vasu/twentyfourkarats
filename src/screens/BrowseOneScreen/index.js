import React from "react";
import "./browseone.css";
// image from assets
import Logo from "../../assets/logo.png";
// icon from react-icons
import { FaCannabis } from "react-icons/fa";

class BrowseOneScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      screen: "browse One",
      data: []
    };
  }

  //   move on next screen function
  moveOnNextScreen = categoyObj => {
    this.props.history.push("browse_two", categoyObj);
  };

  //   api fetch inside componentdidmount
  componentDidMount() {
    fetch("http://24k.etangle.ca/category.json")
      .then(response => response.json())
      .then(json => {
        this.setState({
          data: json.data
        });
      })
      .catch(err => {
        alert(err.message);
      });
  }

  render() {
    return (
      <div className="browseOneContainer">
        <img src={Logo} alt="logo" className="browseOneLogo" />
        <div className="browseOneCards">
          {this.state.data &&
            this.state.data.map((val, i) => {
              return (
                <div
                  className="browseOneItem"
                  key={val._id}
                  onClick={() => this.moveOnNextScreen(val)}
                >
                  <FaCannabis
                    className="browseOneIcon"
                    size={70}
                    color="#783011"
                  />
                  <h1>{val.name}</h1>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default BrowseOneScreen;
