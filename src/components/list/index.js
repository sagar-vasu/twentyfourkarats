import React from "react";
import "./list.css";
import { FaTrash } from "react-icons/fa";

class List extends React.Component {
  render() {
    let data = this.props.data;
    return (
      <tr className="listData" onClick={this.props.onClick}>
        <td className="listInfo">
          <span style={{ marginRight: "10px" }}>
            <FaTrash />
          </span>
          {data.strain}
        </td>
        <td className="listInfo">{data.brand}</td>
        <td>
          <span className="listBadge">{data.specie}</span>
        </td>

        <td>
          <span className="listBadge">{data.cbd}</span>
        </td>
        <td>
          <span className="listBadge">{data.thc}</span>
        </td>
        <td className="listInfo">{data.weight}</td>
        <td className="listInfo">{data.price}</td>
      </tr>
    );
  }
}

export default List;
