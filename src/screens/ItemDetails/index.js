import React from "react";
import BatIcon from "./bat.png";
import "./itemdetails.css";
import { FaArrowLeft, FaShoppingBasket } from "react-icons/fa";

export default class ItemDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      productData: {}
    };
  }

  componentDidMount() {
    this.setState({
      productData: this.props.location.state
    });
  }

  saveProduct = () => {
    let { productData } = this.state;
    let getData = localStorage.getItem("products");
    if (getData) {
      let data = JSON.parse(getData);
      data.push(productData);
      localStorage.setItem("products", JSON.stringify(data));
      this.props.history.push("/review");
    } else {
      let data = [];
      data.push(productData);
      localStorage.setItem("products", JSON.stringify(data));
      this.props.history.push("/review");
    }
  };

  render() {
    let { productData } = this.state;
    return (
      <div className="_container">
        {/* header */}
        <div className="_header">
          <div className="div_1">
            <button
              className="_back_btn"
              onClick={() => this.props.history.goBack()}
            >
              <FaArrowLeft
                style={{ marginTop: "10px" }}
                size={30}
                color="#783011"
              />
              <h4>Back</h4>
            </button>
          </div>
          <div className="div_2">
            <div className="_heading">{productData && productData.strain}</div>
            <div className="_title">{productData && productData.brand}</div>
          </div>
          <div className="div_3">
            <span>${productData && productData.price}</span>
          </div>
        </div>
        <div className="_mainDiv">
          <p className="_discription">
            {productData && productData.description}
          </p>
          <table className="detailsTable">
            <thead>
              <tr>
                <th scope="col" className="detailsHeadings">
                  {productData && productData.strain}
                </th>
                <th scope="col" className="detailsHeadings">
                  {productData && productData.brand}
                </th>
                <th scope="col" className="detailsHeadings">
                  {productData && productData.specie}
                </th>
                <th scope="col" className="detailsHeadings">
                  <span className="badges">
                    {productData && productData.cbd}
                  </span>
                </th>
                <th scope="col" className="detailsHeadings">
                  <span className="badges">
                    {productData && productData.thc}
                  </span>
                </th>
                <th scope="col" className="detailsHeadings">
                  <span className="badges">
                    {productData && productData.weight}
                  </span>
                </th>
                <th scope="col" className="detailsHeadings">
                  {productData && productData.price}
                </th>
              </tr>
            </thead>
          </table>

          <div className="_footerSection">
            <div className="div_a">
              <img
                src={BatIcon}
                alt="bat Icon"
                style={{ marginTop: 30, marginLeft: 30 }}
              />
            </div>
            <div className="div_b">
              <div className="_one">
                <span className="_title_heading">
                  {productData && productData.thc} THC
                </span>
                <span className="_title_heading">
                  {productData && productData.specie}
                </span>
              </div>
              <div className="_three">
                <button
                  className="_orderBtn"
                  onClick={() => this.saveProduct()}
                >
                  <FaShoppingBasket size={30} color="#783011" />
                  <span>ORDER</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
