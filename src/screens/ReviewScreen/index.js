import React from "react";
import "./review.css";
// icons from react-icon
import { FaArrowLeft, FaFastForward } from "react-icons/fa";
// components
import { List } from "../../components";

export default class RreviewScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      products: []
    };
  }

  goBack = () => {
    this.props.history.goBack();
  };

  componentDidMount() {
    let getData = localStorage.getItem("products");
    if (getData) {
      var totalPrice = 0;
      let products = JSON.parse(getData);
      for (let i = 0; i < products.length; i++) {
        totalPrice = products[i].price + totalPrice;
      }
      this.setState({
        products,
        totalPrice: totalPrice.toFixed(2)
      });
    }
  }

  render() {
    return (
      <div className="_container">
        {/* header */}
        <div className="reviewHeader">
          <div className="reviewBack" onClick={() => this.goBack()}>
            <FaArrowLeft className="reviewIcon" size={30} color="#783011" />
            <h4>Back</h4>
          </div>
          <h1>MY ORDER</h1>
        </div>

        {/* section one */}
        <div className="_mainDiv">
          <table className="reviewTable">
            <thead>
              <tr>
                <th scope="col" className="reviewHeadings">
                  STRAIN
                </th>
                <th scope="col" className="reviewHeadings">
                  BRAND
                </th>
                <th scope="col" className="reviewHeadings">
                  TYPE
                </th>
                <th scope="col" className="reviewHeadings">
                  CBD
                </th>
                <th scope="col" className="reviewHeadings">
                  THC
                </th>
                <th scope="col" className="reviewHeadings">
                  AMT
                </th>
                <th scope="col" className="reviewHeadings">
                  PRICE
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.products &&
                this.state.products.map((val, i) => {
                  return <List data={val} key={i} />;
                })}
            </tbody>
          </table>

          {/* section three */}
          <section className="_section_three">
            <h5>
              Your order detail is:{" "}
              <span className="_price">${this.state.totalPrice} </span>
            </h5>
            <p>plus application taxes,</p>
            <p>
              Price in -app are for estimating purposes on, due to possibility
              of software error or delay between app and in store ststem, in the
              event of a difference between app and in-store prices, in store
              price will be applied.
            </p>
          </section>

          <div className="reviewfooter">
            <div
              className="reviewHome"
              onClick={() =>
                this.props.history.push("my_order", this.props.location.state)
              }
            >
              <FaFastForward className="reviewIcon" size={30} color="#783011" />
              <h2>REVIEW</h2>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
