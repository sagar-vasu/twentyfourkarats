import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  HomeScreen,
  BrowseOneScreen,
  SearchScreen,
  ConfirmScreen,
  BrowseTwoScreen,
  itemDetailsScreen,
  MyOrderScreen,
  ReviewScreen,
  SearchResultScreen
} from "../../screens";
import { NotFound } from "../../components";

class BasicRouter extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path="/" component={HomeScreen} />
            <Route path="/browse_one" component={BrowseOneScreen} />
            <Route path="/search" component={SearchScreen} />
            <Route path="/confirm" component={ConfirmScreen} />
            <Route path="/browse_two" component={BrowseTwoScreen} />
            <Route path="/item_details" component={itemDetailsScreen} />
            <Route path="/review" component={ReviewScreen} />
            <Route path="/search_result" component={SearchResultScreen} />
            <Route path="/my_order" component={MyOrderScreen} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default BasicRouter;
